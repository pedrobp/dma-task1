package com.example.pedro.c7155062task1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private EditText nameIn;
    private EditText numberIn;

    private TextView output;

    private ImageView pic;

    private String nameString;
    private int pGuess;

    private int guessThis;
    private Random r;

    private int lives;

    private String correct = "correct";
    private String wrong = "wrong";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //point to the opbject in the interface
        nameIn = (EditText) findViewById(R.id.nameIn);
        numberIn = (EditText) findViewById(R.id.numberIn);
        output = (TextView) findViewById(R.id.output);
        pic = (ImageView) findViewById(R.id.pic);

    }

    public void startGame(View v){
        Random r = new Random();
        guessThis = r.nextInt(20)+1;
        output.setText(Integer.toString(guessThis));
        pic.setVisibility(View.INVISIBLE);

        lives = 3;
    }

    public void guessClick(View v){

        //numberIn.setText("hi", TextView.BufferType.EDITABLE);

        if(lives<1){
        }

        nameString = nameIn.getText().toString();
        pGuess = Integer.parseInt(numberIn.getText().toString());

        if(lives <= 0){
            output.setText("You lose...");
            output.setTextColor(getResources().getColor(R.color.red));
            pic.setImageResource(R.drawable.gameover);
            pic.setVisibility(View.VISIBLE);
        }else {
            if (pGuess < guessThis) {
                lives--;

                output.setText("Too Small");

                pic.setImageResource(R.drawable.cat);
                pic.setVisibility(View.VISIBLE);

                if(guessThis - pGuess <= 3){
                    output.setTextColor(getResources().getColor(R.color.green));
                }
                if(guessThis - pGuess > 3){
                    output.setTextColor(getResources().getColor(R.color.yellow));
                }
                if(guessThis - pGuess > 6){
                    output.setTextColor(getResources().getColor(R.color.red));
                }
            }
            if (pGuess > guessThis) {
                lives--;

                output.setText("Too Big");

                pic.setImageResource(R.drawable.dog);
                pic.setVisibility(View.VISIBLE);

                if(pGuess - guessThis <= 3){
                    output.setTextColor(getResources().getColor(R.color.green));
                }
                if(pGuess - guessThis > 3){
                    output.setTextColor(getResources().getColor(R.color.yellow));
                }
                if(pGuess - guessThis > 6){
                    output.setTextColor(getResources().getColor(R.color.red));
                }
            }
            if (pGuess == guessThis) {
                output.setText("Correct!");

                pic.setImageResource(R.drawable.thumbsup);
                pic.setVisibility(View.VISIBLE);
            }
        }
    }
}
